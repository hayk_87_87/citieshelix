<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => '/', 'uses' => 'InsertUpdateController@index'], function () {
    return View::make('index');
});

Route::get('/compareInsertUpdate', ['as' => 'compareInsertUpdate', 'uses' => 'InsertUpdateController@compareInsertUpdate']);

Route::get('autocomplete', array('as' => 'autocomplete', 'uses' => 'AutoCompleteController@index'));
Route::get('searchajax', ['as' => 'searchajax', 'uses' => 'AutoCompleteController@searchResponse']);


Route::get('latLngShowOnMap', ['as' => 'latLngShowOnMap', 'uses' => 'AutoCompleteController@getLatLngShowOnMap']);
