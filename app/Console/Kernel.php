<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        // The schedule task management need to be configurate on server for a schedule run (call) by every minute, but at first for the process of inserting all data from csv file into db table with 354,596 rows will take more than 1,5-2 minutes
        // $schedule->call('App\Http\Controllers\InsertUpdateController@compareInsertUpdate')->everyMinute()->withoutOverlapping();
        $schedule->call('App\Http\Controllers\InsertUpdateController@compareInsertUpdate')->everyHour()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
