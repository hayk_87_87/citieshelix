<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DOMDocument;
use File;

class InsertUpdateController extends Controller
{
    public function __construct()
    {
        Header("Content-Type:text/html;charset=utf8");
        ini_set("auto_detect_line_endings", true);
    }

    public function index()
    {
        return view('index');
    }

    public function compareInsertUpdate()
    {
        $msg = '';

        $cities = DB::table('cities')->get();

        $array_problems = array();

        if (count($cities) < 1) {

            $filename = $filenameFilePath = public_path() . "/RU.csv";

            $lines2 = file($filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);//new

            $array_insert = array();

            foreach ($lines2 as $key => $newFileCurrLine) {

                $key1 = explode(';', rtrim($lines2[0], ","));
                $value1 = explode(';', rtrim($lines2[$key], ","));

                if (count($key1) != count($value1)) {
                    $array_problems[] = $key;   // this array shows the geonameid of the rows (file lines) which number of columns does not match with the number of db table columns and need to correct it/them handly
                } else {
                    $array_insert[] = array_combine($key1, $value1);
                }
            }

            set_time_limit(0);

            foreach ($array_insert as $array) {
                DB::table('cities')->insert($array);
            }

            $msg = "Data successfully inserted!";
        } else {
            // compare reserve and current working csv files and backup last changed
            $filename = url('/') . '/public/RU.csv';
            $base = url('/') . '/public/RU_reserve.csv';

            $filename = $filenameFilePath = public_path() . "/RU.csv";
            $base = $baseFilePath = public_path() . "/RU_reserve.csv";

            $filename_md5 = (file_exists($filenameFilePath)) ? md5(file_get_contents($filenameFilePath)) : "";
            $base_md5 = (file_exists($baseFilePath)) ? md5(file_get_contents($baseFilePath)) : "";

            $file_changed = false;

            if (strcasecmp($filename_md5, $base_md5) != 0) {

                $file_changed = true;
            }

            if ($file_changed == true) {

                $lines1 = file($base, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);//old
                $lines2 = file($filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);//new

                $array_changes = array();

                $count_lines1 = count($lines1);
                $count_lines2 = count($lines2);

                if ($count_lines1 > $count_lines2) {
                    foreach ($lines2 as $key => $newFileCurrLine) {

                        if (md5($newFileCurrLine) !== md5($lines1[$key])) {

                            $key1 = explode(';', rtrim($lines2[0], ","));
                            $value1 = explode(';', rtrim($lines2[$key], ","));

                            if (count($key1) != count($value1)) {
                                $array_problems[] = $key;   // this array shows the geonameid of the rows which number of columns does not match and need to correct it/them handly
                            } else {
                                $array_changes[] = array_combine($key1, $value1);
                            }
                        }
                    }
                } else {
                    foreach ($lines1 as $key => $oldFileCurrLine) {

                        if (md5($oldFileCurrLine) !== md5($lines2[$key])) {

                            $key1 = explode(';', rtrim($lines2[0], ","));
                            $value1 = explode(';', rtrim($lines2[$key], ","));

                            $array_changes[] = array_combine($key1, $value1);
                        }
                    }
                    if ($count_lines2 > $count_lines1) {

                        for ($j = $count_lines1 - 1; $j < $count_lines2; $j++) {

                            $key1 = explode(';', rtrim($lines2[0], ","));
                            $value1 = explode(';', rtrim($lines2[$j], ","));

                            if (count($key1) != count($value1)) {
                                $array_problems[] = $key;   // Note: [array problems]: This array shows the geonameid of the rows which number of columns does not match and need to correct it/them handly
                            } else {
                                $array_changes[] = array_combine($key1, $value1);
                            }
                        }
                    }
                }

                File::delete($baseFilePath);
                $success = File::copy($filenameFilePath, $baseFilePath);

                // update all changed in csv file data on db
                set_time_limit(0);
                foreach ($array_changes as $array) {
                    DB::table('cities')
                        ->where('geonameid', $array['geonameid'])
                        ->update($array);
                }

                $msg = "Data successfully updated!";
            } else {
                $msg = "Data successfully compared. Nothing inserted/updated!";
            }
        }

        $array_problems['message'] = $msg;

        // return redirect('/autocomplete')->with('message' => $msg);
        return redirect('/autocomplete')->with('array_problems', $array_problems);

    }
}
