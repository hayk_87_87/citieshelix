<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use DB;

class AutoCompleteController extends Controller
{
    public function index()
    {
        $data = array();
        $countryname = "";

        return view('autocomplete.index', ['markers' => $data, 'countryname' => $countryname]);
    }

    public function searchResponse(Request $request)
    {
        $query = $request->get('term', '');

        $cities = City::where('name', 'LIKE', '%' . $query . '%')->take(20)->get();

        $data = array();
        foreach ($cities as $city) {
            $data[] = array(
                'name' => htmlspecialchars($city->name, ENT_NOQUOTES, "UTF-8"),
                'country' => htmlspecialchars($city->country, ENT_NOQUOTES, "UTF-8"),
                'lat' => floatval(htmlspecialchars($city->latitude, ENT_NOQUOTES, "UTF-8")),
                'lng' => floatval(htmlspecialchars($city->longitude, ENT_NOQUOTES, "UTF-8"))
            );
        }

        if (count($data))
            return $data;
        else
            return ['name' => '', 'country' => '', 'value' => 'No Result Found', 'id' => ''];
    }

    public function getLatLngShowOnMap(Request $request)
    {
        $lng = $request->longitude;
        $lat = $request->latitude;

        $cities_ = City::select(DB::raw('*, ( 6367 * acos( cos( radians(' . $lat . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians( latitude ) ) ) ) AS distance'))
            ->orderBy('distance')
            ->take(20)
            ->get();

        $data = array();
        foreach ($cities_ as $city_) {
            $data[] = array(
                'name' => htmlspecialchars($city_->name, ENT_NOQUOTES, "UTF-8"),
                'country' => htmlspecialchars($city_->country, ENT_NOQUOTES, "UTF-8"),
                'lat' => floatval(htmlspecialchars($city_->latitude, ENT_NOQUOTES, "UTF-8")),
                'lng' => floatval(htmlspecialchars($city_->longitude, ENT_NOQUOTES, "UTF-8"))
            );
        }

        $countryname = $request->input('countryname');

        return view('autocomplete.index', ['markers' => $data, 'countryname' => $countryname[0]]);
    }
}
