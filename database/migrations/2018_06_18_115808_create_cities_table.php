<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            // $table->char('id', 16)->charset('binary')->primary();
            // $table->Increments('id');
            $table->string('geonameid')->primary();
            // $table->string('name')->charset('binary');
            // $table->longText('name');
            $table->binary('name');
            $table->binary('asciiname');
            $table->binary('alternatenames');
            $table->string('latitude')->charset('binary');
            $table->string('longitude')->charset('binary');
            $table->string('feature_class');
            $table->string('feature_code');
            $table->string('country');
            $table->string('cc2');
            $table->string('admin1')->charset('binary');
            $table->string('admin2')->charset('binary');
            $table->string('admin3')->charset('binary');
            $table->string('admin4')->charset('binary');
            $table->string('population');
            $table->string('elevation');
            $table->string('dem');
            $table->string('timezone');
            $table->string('modification_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
