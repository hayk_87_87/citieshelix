@extends('layouts.default')

@section('content')
    @if(session()->has('array_problems'))

        @foreach(session()->get('array_problems') as $key => $value)
            @if(!is_numeric($key))
                <div class="success-message alert alert-success">
                    {{ $value }}<br>
                </div>
            @endif
        @endforeach

        @if(count(session()->get('array_problems')) > 1)
            <h6><b>Note: [array problems]: This array shows the geonameid of the rows which number of columns does not
                    match and need to correct
                    it/them handly</b></h6>
            <div class="warning-message alert alert-warning">
                @foreach(session()->get('array_problems') as $key => $value)
                @if(is_numeric($key))
                {{ $key . ' => ' . $value }}</br>
                @endif
                @endforeach
            </div>
        @endif

    @endif

    <h1>Laravel 5 - Autocomplete Mutiple Fields Using jQuery, Ajax and MySQL</h1>

    <form action="{{route('latLngShowOnMap')}}" method="get">

        <table class="table table-bordered">
            <tr>
                <th>City Name <i>(For Example: <u>Zyabrikovo, Znamenka, Zhukovo</u>)</i></th>
            </tr>
            <tr>
                <td><input placeholder="Please type city name" class="form-control autocomplete_txt"
                           value="{!! $countryname  !!}" type='text'
                           data-type="cityname" id='countryname_1' name='countryname[]'/></td>
            </tr>
        </table>

        <input class="form-control autocomplete_txt" type='hidden' longitude='' id='longitude' name='longitude'/>
        <input class="form-control autocomplete_txt" type='hidden' latitude='' id='latitude' name='latitude'/>

        <button type="submit" class='btn btn-warning'>Show 20 nearest cities</button>

    </form>

    <script type="text/javascript">

        $(document).on('focus', '.autocomplete_txt', function () {
            type = $(this).data('type');

            if (type == 'cityname') autoType = 'name';

            $(this).autocomplete({
                minLength: 1,
                source: function (request, response) {
                    $.ajax({
                        url: "{{ route('searchajax') }}",
                        dataType: "json",
                        data: {
                            term: request.term,
                            type: type,
                        },
                        success: function (data) {
                            var array = $.map(data, function (item) {
                                return {
                                    label: item[autoType],
                                    value: item[autoType],
                                    data: item
                                }
                            });
                            response(array)
                        }
                    });
                },
                select: function (event, ui) {
                    var data = ui.item.data;
                    id_arr = $(this).attr('id');
                    id = id_arr.split("_");
                    elementId = id[id.length - 1];
                    $('#countryname_' + elementId).val(data.name);

                    $('#longitude').val(data.lng);
                    $('#latitude').val(data.lat);
                }
            });

        });
    </script>


    {{--automplete search reults on the gooogle map--}}

    <link href="{{ asset('public/css/map.css') }}" rel="stylesheet">

    <script>
        var markers = JSON.parse('{!! (json_encode($markers))  !!}');

        window.onload = function () {
            if (markers.length > 0) {
                LoadMap(markers);

            }
        }
    </script>

    <script type="text/javascript" src="{{ asset('public/js/map.js') }}"></script>


    <div id="dvMap" style=""></div>

@endsection