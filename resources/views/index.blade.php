@extends('layouts.default')

@section('content')

    <h1>Laravel 5 - Autocomplete Mutiple Fields Using jQuery, Ajax and MySQL</h1>

    <script type="text/javascript">
        function toggle_visibility(obj) {
            var e = obj;
            if (e.style.display == 'block')
                e.style.display = 'none';
            else
                e.style.display = 'block';
        }
    </script>

    <div align="center">

        <marquee direction="down" width="800" height="400" behavior="alternate" style="border:solid">
            <marquee behavior="alternate">
                Please wait untill your data files will be compared and insert/update on database!
            </marquee>
        </marquee>

        <br>

        <a href="{{route('compareInsertUpdate')}}" onclick="toggle_visibility(this);" style="display: block"><h1> ---
                >>> Start Autocomplete <<<-- </h1></a>

    </div>

@endsection