<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Laravel 5 - Autocomplete Mutiple Fields Using jQuery, Ajax and MySQL</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

{{--https://console.developers.google.com/apis/credentials?project=citieshelix-1529613259827&authuser=0--}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBox3TvUA_j5u4bDmVLXvbtzO7F6y19piA" async defer></script>
{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAS0MutNxxp5INQTE4AkxRDO2WWXffBhC8&callback=initMap" async defer></script>--}}
